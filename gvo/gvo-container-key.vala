/* gvls-container-key.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Is an object key to peekup objects in a
 * {@link GVo.Container}, of the same type
 * required by the added object if it was a
 * {@link GVo.ContainerHashable}, for specialized
 * objects see related interfaces.
 */
public interface GVo.ContainerKey : GLib.Object
{
  /**
   * Returns a string representing the key
   */
  public virtual string? get_string () {
    if (this is ContainerKeyString) {
        var c = this as ContainerKeyString;
        if (c != null) {
            return c.key;
        }
    }

    return null;
  }
  /**
   * Returns a integer representing the key
   */
  public virtual int    get_integer () {
    if (this is ContainerKeyInteger) {
        var c = this as ContainerKeyInteger;
        return c.key;
    }

    return -1;
  }
  public virtual  GLib.Object get_object () { return this; }
}

/**
 * Is a string kind key implementing {@link ContainerKey}
 */
public interface GVo.ContainerKeyString : GLib.Object, ContainerKey
{
  /**
   * A string representing the key
   */
  public abstract string key { get; set; }
}

/**
 * Is a integer kind key implementing {@link ContainerKey}
 */
public interface GVo.ContainerKeyInteger : GLib.Object, ContainerKey
{
  /**
   * A integer representing the key
   */
  public abstract int key { get; set; }
}

/**
 * An object implementing {@link ContainerHashable}
 * will be added to internal {@link Container}'s hash
 * table and can be retrieved using a {@link GVo.ContainerKey}
 * object
 */
public interface GVo.ContainerHashable : GLib.Object {
  /**
   * Calculates the hash of the object.
   */
  public abstract uint hash ();
  /**
   * Checks if the object has the same hash of
   * another or if they are pointing to the same.
   */
  public abstract bool equal (GLib.Object obj);
}

