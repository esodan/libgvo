/* gvls-duration.vala
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A object representing a duration according to
 * ISO 8601 duration, e.g. “P1DT18H”
 */
[Version (since="2.0")]
public class GVo.Duration : GLib.Object, GVo.Object
{
    public string val { get; set; }

    /**
     * Creates a new duration expression for the
     * one between date start (ds) and date end (de)
     */
    public Duration.from_dates (GLib.Date ds, GLib.Date de) {
        dates_duration (ds, de);
    }

    /**
     * Creates a new duration expression for the
     * one between timestampo start (dts) and timestamp end (dte)
     */
    public Duration.from_timestamps (GLib.DateTime dts, GLib.DateTime dte)
    {
        timestamps_duration (dts, dte);
    }

    /**
     * Duration in years
     */
    public void get_duration_parts (out int years,
                                    out int months,
                                    out int days,
                                    out int hours,
                                    out int minutes,
                                    out int seconds)
    {
        years = 0;
        months = 0;
        days = 0;
        hours = 0;
        minutes = 0;
        seconds = 0;
        if (val == null || val == "") {
            return;
        }
        unichar c;
        int index = 0;
        string n = "";
        bool m = false;
        while (val.get_next_char (ref index, out c)) {
            n += c.to_string ();
            if (c == 'P') {
                n = "";
                continue;
            }

            if (c == 'T') {
                n = "";
                continue;
            }


            if (c == 'Y') {
                years = int.parse (n);
                n = "";
                m = true;
                continue;
            }

            if (c == 'M') {
                if (m) {
                    months = int.parse (n);
                    m = false;
                } else {
                    minutes = int.parse (n);
                }
                n = "";
                continue;
            }

            if (c == 'D') {
                days = int.parse (n);
                n = "";
                continue;
            }

            if (c == 'H') {
                hours = int.parse (n);
                n = "";
                continue;
            }

            if (c == 'S') {
                seconds = int.parse (n);
                break;
            }
        }
    }

    /**
     * Calculates the duration between two timestamps and parse it
     * to set its string representation using ISO  8601
     */
    public void timestamps_duration (GLib.DateTime dts, GLib.DateTime dte)
    {
        int64 d = 0;
        int64 h = 0;
        int64 m = 0;
        int64 s = 0;

        timestamps_diference (dts, dte, out d, out h, out m, out s);

        var dd = dts.add_days ((int) d);
        int y = dd.get_year () - dts.get_year ();

        var dy = dts.add_years (y);
        timestamps_diference (dy, dte, out d, out h, out m, out s);
        int mt = dte.get_month () - dy.get_month ();

        var dm = dy.add_months (mt);
        timestamps_diference (dm, dte, out d, out h, out m, out s);
        int dt = (int) d;
        var ddd = dm.add_days (dt);
        timestamps_diference (ddd, dte, out d, out h, out m, out s);
        int ht = (int) h;
        var ddh = ddd.add_hours (ht);
        timestamps_diference (ddh, dte, out d, out h, out m, out s);
        int mit = (int) m;
        var ddm = ddh.add_minutes (mit);
        timestamps_diference (ddm, dte, out d, out h, out m, out s);
        int st = (int) s;

        val = "P%dY%dM%dDT%dH%dM%dS".printf (y, mt, dt, ht, mit, st);
    }

    /**
     * Calculates the duration between two timestamps and parse it
     * to set its string representation using ISO  8601
     */
    public void dates_duration (GLib.Date ds, GLib.Date de)
    {
        if (!ds.valid () || !de.valid ()) {
            val = "P0Y0M0D";
        }

        var dts = new GLib.DateTime (new TimeZone.utc (),
                                ds.get_year (),
                                ds.get_month (),
                                ds.get_day (),
                                0, 0, 0.0);
        var dte = new GLib.DateTime (new TimeZone.utc (),
                                de.get_year (),
                                de.get_month (),
                                de.get_day (),
                                0, 0, 0.0);
        timestamps_duration (dts, dte);
    }

    /**
     * Calculates the duration in days, hours, minutes and seconds
     * between two ti
    public virtual GLib.VariantType variant_type_parseable () {
        return GLib.VariantType.DICTIONARY;
    }mestamps
     *
     * @param d Duration in days
     * @param h Duration in hours after duration of days
     * @param m Duration in minutes after duration of days
     * @param s Duration in seconds after duration of days
     */
    public void timestamps_diference (GLib.DateTime dts, GLib.DateTime dte,
                                        out int64 d, out int64 h,
                                        out int64 m, out int64 s)
    {
        d = h = m = s = 0;
        GLib.TimeSpan t = dte.difference (dts);
        s = t / 1000000;
        m = s / 60;
        h = m / 60;
        d = h / 24;
    }

    internal GLib.Variant to_variant () {
        var v = new GLib.Variant.string (val);
        return v;
    }

    internal void parse_variant (GLib.Variant v)
        requires (v.is_of_type (GLib.VariantType.STRING))
    {
        val = v.get_string ();
    }

    internal bool is_parseable (GLib.Variant v) {
        return v.is_of_type (GLib.VariantType.STRING);
    }

    internal GLib.List<GLib.VariantType> variant_types_parseable () {
        GLib.List<GLib.VariantType> l = new GLib.List<GLib.VariantType> ();
        l.append (GLib.VariantType.STRING);
        return l;
    }
}

