/* gvls-container.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * A generic {@link GLib.Object} container.
 *
 * Its items can be accessed using {@link GLib.ListModel}
 * interface and by a key in a hash table.
 *
 * Hash key is a {@link GLib.Object} of the type {@link ContainerKey}.
 *
 * As a {@link GVo.Object} can used to hold an array
 * of objects in a JSON serialization of a set of items
 * of the {@link GVo.Object} kind
 */
public interface GVo.Container : GLib.Object,
                                GLib.ListModel,
                                GVo.Object
{
  /**
   * The {@link GLib.Type} of the objects in the collection.
   *
   * If not set, it will be set using the {@link GLib.Type}
   * of the first object added using {@link add}
   */
  public abstract GLib.Type object_type { get; }

  /**
   * Adds a new object to the collection, if the object
   * implements {@link GVo.ContainerHashable} the object
   * will be taken from an internal hash table, speeding up
   * it search.
   */
  public abstract void add (GLib.Object object);
  /**
   * Search an object using its pointer or seeking the
   * internal hash table if it is a {@link GVo.ContainerHashable}
   */
  public abstract GLib.Object? find (GLib.Object key);
  /**
   * Removes an object from the collection
   */
  public abstract void remove (GLib.Object object);
}
