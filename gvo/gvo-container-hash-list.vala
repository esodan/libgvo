/* gvls-container-hash-list.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Gee;

/**
 * A {@link Container} implementation
 */
public class GVo.ContainerHashList : Gee.ArrayList<GLib.Object>,
                              GLib.ListModel,
                              GVo.Object,
                              GVo.Container
{
  private Gee.TreeMap<uint,GLib.Object> _map = new Gee.TreeMap<uint,GLib.Object> ();
  private GLib.Type _object_type = GLib.Type.INVALID;

  internal GLib.Type object_type { get { return _object_type; } }

  /**
   * Creates a new {@link Container} with an specific
   * {@link GLib.Type}.
   *
   * If the collection is serialized to a {@link GLib.Variant}
   * will use {@link Container.object_type} to instantiate
   * back a deserialization process, so take care to add
   * object of the same kind for serialization accuracy.
   */
  public ContainerHashList.for_type (GLib.Type type) {
    _object_type = type;
  }

    /**
     * Removes all items in the collection
     */
    [Version (since="0.14.0")]
    public void remove_all () {
        clear ();
        _map.clear ();
    }

  internal new void add (GLib.Object object) {
    if (_object_type == GLib.Type.INVALID) {
      _object_type = object.get_type ();
    }
    var a = this as Gee.ArrayList<GLib.Object>;
    if (a == null) {
        return;
    }

    if (_object_type.is_a (typeof (ContainerHashable))) {
        var ho = object as ContainerHashable;
        if (ho != null) {
            if (_map.has_key (ho.hash ())) {
                var ro = _map.get (ho.hash ());
                if (ro != null) {
                    a.remove (ro);
                }
            }
            _map.@set (ho.hash (), object);
            a.add (object);
        }
    } else {
        a.add (object);
    }
  }

  internal new void remove (GLib.Object object) {
    var a = this as Gee.ArrayList<GLib.Object>;
    for (int i = 0; i < get_n_items (); i++) {
      GLib.Object ob = get_item (i);
      if (ob == object) {
        a.remove (ob);
      }
      // if (object is GVls.Symbol && ob is GVls.Symbol) {
      //   var sym = object as GVls.Symbol;
      //   if (sym != null) {
      //       _map.unset (sym.full_name);
      //       var osym = ob as GVls.Symbol;
      //       if (osym != null) {
      //           if (osym.name == sym.full_name) {
      //             a.remove (ob);
      //           }
      //       }
      //   }
      // }
      if (object is ContainerHashable && ob is ContainerHashable) {
        var c = object as ContainerHashable;
        var oh = object as ContainerHashable;
        _map.unset (c.hash ());
        var oc = ob as ContainerHashable;
        if (oc.hash() == oh.hash ()) {
            a.remove (ob);
        }
      }
    }
  }

  internal GLib.Object? find (GLib.Object key) {
    if (key is ContainerKeyString) {
        var k = key as ContainerKeyString;
        if (k != null) {
            return _map.@get (GLib.str_hash (k.key));
        }
    }
    return @get (index_of (key));
  }

  internal GLib.Object? get_item (uint position) {
    return @get ((int) position);
  }
  internal GLib.Type get_item_type () { return typeof (GLib.Object); }
  internal uint get_n_items () { return (uint) size; }

  // Object Variant
  internal GLib.Variant to_variant () {
    var b = new GLib.VariantBuilder (new GLib.VariantType ("av"));
    for (uint i = 0; i < get_n_items (); i++) {
      GLib.Object obj = get_item (i);
      if (obj is GVo.Object) {
        b.add ("v", ((GVo.Object) obj).to_variant ());
      }
    }
    return b.end ();
  }
  internal void parse_variant (GLib.Variant v)  {
    if (!object_type.is_a (typeof (GVo.Object))) {
        return;
    }

    if (!object_type.is_instantiatable  ()) {
      return;
    }

    var iter = v.iterator ();
    for (int i = 0; i < iter.n_children (); i++) {
      GLib.Variant current = iter.next_value ().get_variant ();
      var obj = GLib.Object.new (object_type) as GVo.Object;
      obj.parse_variant (current);
      add (obj);
    }
  }

    internal bool is_parseable (GLib.Variant v) {
        if (!v.is_container ()) {
            return false;
        }

        if (v.check_format_string ("av", false)) {
            return true;
        }

        if (v.check_format_string ("a{sv}", false)) {
            return true;
        }

        return false;
    }
}
