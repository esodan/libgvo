/* gvls-container-sorted-list.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Gee;


/**
 * Allows compare the object against other {@link GLib.Object}
 */
[Version (since="0.16.0")]
public interface GVo.Comparable : GLib.Object
{
    /**
     * Compares to a {@link GLib.Object}
     *
     * If the object is not as {@link GVo.Comparable} then
     * this methods always state it is bigger than this.
     *
     * If both are {@link GVo.StringComparable} they use its
     * {@link GVo.StringComparable.get_comparable_string} to compare each other.
     *
     * By default this method returns this object is lower than the provided one.
     *
     * @param b a {@link GLib.Object} to compare against
     */
    public virtual int compare (GLib.Object b) {
        if (!(b is Comparable)) {
            return 1;
        }
        if ((this is GVo.StringComparable) && (b is GVo.StringComparable)) {
            return GLib.strcmp (((GVo.StringComparable) this).get_comparable_string (), (((GVo.StringComparable) b).get_comparable_string ()));
        }
        return -1;
    }
}

/**
 * A {@link GVo.Comparable} using a string for comparation
 */
[Version (since="0.16.0")]
public interface GVo.StringComparable :    GLib.Object,
                                            GVo.Comparable
{
    /**
     * A string used for comparation purposes
     */
    public abstract string get_comparable_string ();
}

/**
 * A {@link Container} implementation providing a sorted list
 * if {@link object_type} is a {@link GVo.Comparable}
 * and, if {@link object_type} is a {@link GVo.ContainerHashable},
 * the container allows to hold not repeated items, find them
 * using a {@link ContainerKey} in a hash table.
 */
[Version (since="0.16.0")]
public class GVo.ContainerSortedHashList :   GLib.Object,
                                          GLib.ListModel,
                                          GVo.Object,
                                          GVo.Container
{

    internal int compare_objects (GLib.Object? a, GLib.Object? b) {
        if (a == null && b == null) {
            return 0;
        }

        if (a == null && b != null) {
            return -1;
        }

        if (a != null && b == null) {
            return 1;
        }

        if ((a is GVo.Comparable) && (b is GVo.Comparable)) {
            return ((GVo.Comparable) a).compare (b);
        }

        return -1;
    }

    private GLib.Type _object_type = GLib.Type.INVALID;
    private Gee.TreeSet<GLib.Object> tree = new Gee.TreeSet<GLib.Object> ((GLib.CompareDataFunc<GLib.Object>) compare_objects);
    private Gee.HashMap<uint, GLib.Object> keys = new Gee.HashMap<uint,GLib.Object> ();

    // GVo.Container
    internal GLib.Type object_type { get { return _object_type; } }

    /**
    * Creates a new {@link Container} with an specific
    * {@link GLib.Type}.
    *
    * If the collection is serialized to a {@link GLib.Variant}
    * will use {@link Container.object_type} to instantiate
    * back a deserialization process, so take care to add
    * object of the same kind for serialization accuracy.
    */
    public ContainerSortedHashList.for_type (GLib.Type type) {
        _object_type = type;
    }

    /**
     * Removes all items in the collection
     */
    [Version (since="0.14.0")]
    public void remove_all () {
        tree.clear ();
    }

    internal void add (GLib.Object object) {
        if (!(object is Comparable)) {
            return;
        }

        if (_object_type == GLib.Type.INVALID) {
          _object_type = object.get_type ();
        }

        if (object is GVo.ContainerHashable) {
            if (keys.has_key (((GVo.ContainerHashable) object).hash ())){
                return;
            } else {
                keys.set (((GVo.ContainerHashable) object).hash (), object);
            }
        }

        tree.add (object);
    }

    internal void remove (GLib.Object object) {
        tree.remove (object);
    }

    internal GLib.Object? find (GLib.Object key) {
        if (object_type.is_a (typeof (GVo.StringComparable))) {
            if (key is ContainerKeyString) {
                var k = key as ContainerKeyString;
                if (k != null) {
                    foreach (GLib.Object obj in tree) {
                        if (((GVo.StringComparable) obj).get_comparable_string () == k.key) {
                            return obj;
                        }
                    }
                }
            }
        }

        return null;
    }

    // GLib.ListModel

    internal GLib.Object? get_item (uint position) {
        uint i = 0;
        foreach (GLib.Object obj in tree) {
            if (i == position) {
                return obj;
            }
            i++;
        }
        return null;
    }
    internal GLib.Type get_item_type () { return typeof (GLib.Object); }
    internal uint get_n_items () { return (uint) tree.size; }

    // Object Variant
    internal GLib.Variant to_variant () {
        var b = new GLib.VariantBuilder (new GLib.VariantType ("av"));
        foreach (GLib.Object obj in tree) {
            if (obj is GVo.Object) {
                b.add ("v", ((GVo.Object) obj).to_variant ());
            }
        }
        return b.end ();
    }
    internal void parse_variant (GLib.Variant v)  {
        if (!object_type.is_a (typeof (GVo.Object))) {
            return;
        }

        if (!object_type.is_instantiatable  ()) {
            return;
        }

        var iter = v.iterator ();
        for (int i = 0; i < iter.n_children (); i++) {
            GLib.Variant current = iter.next_value ().get_variant ();
            var obj = GLib.Object.new (object_type) as GVo.Object;
            obj.parse_variant (current);
            add (obj);
        }
    }

    internal bool is_parseable (GLib.Variant v) {
        if (!v.is_container ()) {
            return false;
        }

        if (v.check_format_string ("av", false)) {
            return true;
        }

        return false;
    }
}
