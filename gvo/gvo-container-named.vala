/* gvo-container-named.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General internal License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General internal License for more details.
 *
 * You should have received a copy of the GNU Lesser General internal License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * A generic {@link GVo.Named} container map with its name string as
 * key. Use this container if you will parse a JSON dictionary of objects
 * with a string property name as key and you know the kind of objects are
 * present in the dictionary so its entries can be parsed to a {@link GVo.Named}
 * objects and its {@link GVo.Named.object}'s type is know too so the
 * entry's value {@link GLib.Variant} can be parsed to object implementing
 * {@link GVo.Object} interface.
 *
 * Its items can be accessed using {@link GLib.ListModel}
 * interface and by a key in a hash table.
 *
 * Hash key is a {@link GVo.ContainerKeyStringHolder} of
 * the type {@link ContainerKeyString}.
 *
 * As a {@link GVo.Named} can be used to hold an array
 * of objects in a JSON serialization of a set of items
 * of the {@link GVo.Object} kind mapped with a string
 * property as key.
 */
[Version (since="2.0")]
public class GVo.ContainerNamed : GLib.Object,
                                GLib.ListModel,
                                GVo.Object,
                                GVo.Container
{
    Gee.HashMap<string,GVo.Object> map = new Gee.HashMap<string,GVo.Object> ();
    Gee.ArrayList<GVo.Object> list = new Gee.ArrayList<GVo.Object> ();
    Gee.HashMap<string,GLib.Type> _types = new Gee.HashMap<string,GLib.Type> ();

    /**
     * Returns a {@link GVo.Named} object with the given name
     * in the collection
     */
    public new GVo.Object? @get (string name) {
        return map.get (name) as GVo.Named;
    }

    /**
     * Add supported {@link GVo.Object} derived types
     */
    public void add_parseable_type (GLib.Type type) {
        if (!type.is_a (typeof (GVo.Object))) {
            return;
        }

        _types.set (type.name (), type);
        debug ("Added type: %s : number: %d", type.name (), _types.size);
    }
    // GVo.Container
    internal GLib.Type object_type { get { return typeof (GVo.Named); } }

    internal void add (GLib.Object object) {
        if (!(object is GVo.Named)) {
            warning (_("This container support only objects of type GVo.Named"));
            return;
        }

        var o = (GVo.Named) object;

        if (o.object == null) {
            warning (_("Adding a GVo.Named object with it's GVo.Object object set to null is invalid"));
            return;
        }

        map.set (o.name, o);
        list.add (o);
    }

    internal void remove (GLib.Object object) {
        if (!(object is GVo.Named)) {
            warning (_("This container support only objects of type GVo.Named"));
            return;
        }

        var o = (GVo.Named) object;
        map.unset (o.name);
        list.remove (o);
    }

    internal GLib.Object? find (GLib.Object key) {
        if (!(key is ContainerKeyStringHolder)) {
            return null;
        }

        var k = (ContainerKeyStringHolder) key;
        return map.get (k.key);
    }

    // GLib.ListModel
    internal GLib.Object? get_item (uint position) {
        return list.@get ((int) position);
    }
    internal GLib.Type get_item_type () { return object_type; }
    internal uint get_n_items () { return (uint) list.size; }

    // GVo.Object
    internal GLib.Variant to_variant () {
        var b = new GLib.VariantBuilder (new GLib.VariantType ("a{sv}"));
        foreach (string k in map.keys) {
            var obj = map.get (k) as GVo.Named;
            if (obj == null) {
                continue;
            }

            GLib.Variant? v = null;
            if (obj.object == null) {
                v = new GLib.Variant.string (obj.name);
            } else {
                v = obj.object.to_variant ();
            }

            b.add ("{sv}", obj.name, v);
        }

        return b.end ();
    }

    internal void parse_variant (GLib.Variant v) {
        if (!v.is_container ()) {
            return;
        }

        GLib.Variant? val = null;
        string? key = null;
        GLib.VariantIter iter = v.iterator ();
        if (_types.size == 0) {
            debug ("No types to parse objects are present");
            return;
        }

        while (iter.next ("{sv}", out key, out val)) {
            GLib.Type parser = GLib.Type.INVALID;
            foreach (GLib.Type type in _types.values) {
                var o = GLib.Object.new (type) as GVo.Object;
                if (!o.is_parseable (val)) {
                    continue;
                }

                parser = o.get_type ();
                break;
            }

            if (parser != GLib.Type.INVALID && parser.is_a (typeof (GVo.Object)) && parser.is_instantiatable ()) {
                GVo.Object o = GLib.Object.new (parser) as GVo.Object;
                o.parse_variant (val);
                GVo.Object no = GLib.Object.new (typeof (GVo.Named), name: key, object: o) as GVo.Object;
                add (no);
            }
        }
    }

    internal bool is_parseable (GLib.Variant v) {
        foreach (GLib.Type type in _types.values) {
            var o = GLib.Object.new (type) as GVo.Object;
            if (o.is_parseable (v)) {
                return true;
            }
        }

        return false;
    }
}
