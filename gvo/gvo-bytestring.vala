/* gvls-integer.vala
 *
 * Copyright 2020 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * A object representing an array of bytes.
 */
[Version (since="2.0")]
public class GVo.ByteString : GLib.Object,
                        GVo.Object
{
    /**
     * An {@link GLib.Bytes} representing binary data
     */
    public GLib.Bytes val { get; set; }

    internal GLib.Variant to_variant () {
        return new GLib.Variant.from_bytes (GLib.VariantType.BYTESTRING, val, true);
    }

    internal void parse_variant (GLib.Variant v)
    {
        if (!v.is_of_type (GLib.VariantType.BYTESTRING)) {
            return;
        }

        val = v.get_data_as_bytes ();
    }


    internal bool is_parseable (GLib.Variant v) {
        if (v.is_of_type (GLib.VariantType.BYTESTRING)) {
            return true;
        }

        return false;
    }
}

