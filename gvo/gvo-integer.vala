/* gvls-integer.vala
 *
 * Copyright 2020 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * A object representing an integer of 64 bits size value
 * usable when you want to avoid a serialization to allwas set
 * an integer, instead you can use this object to set an object's
 * property to null so the output will ignore this value no
 * property in JSON or {@link GLib.Variant} will be serialized as
 * a {@link GLib.VariantType.INT64}.
 *
 * {{{
 *
 * class MyClass: GLib.Object, GVo.Object
 * {
 *     // Provide the output property's name on JSON or Variant using nick
 *     [Description (nick="Intense")]
 *     public GVo.Integer intense { get; set; }
 *
 *     construct {
 *         // This sets 'intense' so the output will allways serialize out
 *         // this property, if you don't wnat so, just set to null
 *         intense = new GVo.Integer ();
 *         intense.val = 10;
 *     }
 * }
 *
 * public class App {
 *     private int main () {
 *         var myobj = new MyClass ();
 *         // This makes 'intense' property to be ignored at serialization
 *         // to Variant
 *         myobj.intense = null;
 *         return 0;
 *     }
 * }
 * }}}
 */
public class GVo.Integer : GLib.Object,
                        GVo.ContainerHashable,
                        GVo.Object
{
    int64 _v = 0;

    public int val {
        get {
            return (int) _v;
        }

        set {
            _v = (int64) value;
        }
    }

    public string to_string () { return _v.to_string (); }

    public GLib.Variant to_variant () {
        return new GLib.Variant.int64 (_v);
    }

    public void parse_variant (GLib.Variant v)
    {
        if (v.is_of_type (GLib.VariantType.INT16)) {
            _v = (int64) v.get_int16 ();
        }

        if (v.is_of_type (GLib.VariantType.INT32)) {
            _v = (int64) v.get_int32 ();
        }

        if (v.is_of_type (GLib.VariantType.INT64)) {
            _v = (int64) v.get_int64 ();
        }
    }


    public uint hash () {
        return GLib.int_hash (val);
    }

    public bool equal (GLib.Object obj) {
        if (!(obj is Integer)) {
            return false;
        }

        var o = obj as Integer;
        return o.val == val;
    }

    internal bool is_parseable (GLib.Variant v) {
        if (v.is_of_type (GLib.VariantType.INT16)) {
            return true;
        }

        if (v.is_of_type (GLib.VariantType.INT32)) {
            return true;
        }

        if (v.is_of_type (GLib.VariantType.INT64)) {
            return true;
        }

        return false;
    }

    internal GLib.List<GLib.VariantType> variant_types_parseable () {
        GLib.List<GLib.VariantType> l = new GLib.List<GLib.VariantType> ();
        l.append (GLib.VariantType.INT64);
        l.append (GLib.VariantType.INT32);
        l.append (GLib.VariantType.INT16);
        return l;
    }
}


[Version (since="2.0")]
public class GVo.Int64 : GVo.Integer {}


[Version (since="2.0")]
public class GVo.Int16 : GLib.Object,
                        GVo.ContainerHashable,
                        GVo.Object
{
    public int16 val { get; set; }

    public string to_string () { return val.to_string (); }

    internal GLib.Variant to_variant () {
        return new GLib.Variant.int16 (val);
    }

    internal void parse_variant (GLib.Variant v)
    {
        if (!v.is_of_type (GLib.VariantType.INT16)) {
            return;
        }

        val = v.get_int16 ();
    }


    internal uint hash () {
        return GLib.int_hash (val);
    }

    internal bool equal (GLib.Object obj) {
        if (!(obj is Int16)) {
            return false;
        }

        var o = obj as Int16;
        return o.val == val;
    }

    internal bool is_parseable (GLib.Variant v) {
        if (v.is_of_type (GLib.VariantType.INT16)) {
            return true;
        }

        return false;
    }

    internal GLib.List<GLib.VariantType> variant_types_parseable () {
        GLib.List<GLib.VariantType> l = new GLib.List<GLib.VariantType> ();
        l.append (GLib.VariantType.INT16);
        return l;
    }
}


[Version (since="2.0")]
public class GVo.Int32 : GLib.Object,
                        GVo.ContainerHashable,
                        GVo.Object
{
    public int32 val { get; set; }

    public string to_string () { return val.to_string (); }

    internal GLib.Variant to_variant () {
        return new GLib.Variant.int32 (val);
    }

    internal void parse_variant (GLib.Variant v)
    {
        if (!v.is_of_type (GLib.VariantType.INT32)) {
            return;
        }

        val = v.get_int32 ();
    }


    internal uint hash () {
        return GLib.int_hash (val);
    }

    internal bool equal (GLib.Object obj) {
        if (!(obj is Int32)) {
            return false;
        }

        var o = obj as Int32;
        return o.val == val;
    }

    internal bool is_parseable (GLib.Variant v) {
        if (v.is_of_type (GLib.VariantType.INT32)) {
            return true;
        }

        return false;
    }

    internal GLib.List<GLib.VariantType> variant_types_parseable () {
        GLib.List<GLib.VariantType> l = new GLib.List<GLib.VariantType> ();
        l.append (GLib.VariantType.INT32);
        l.append (GLib.VariantType.INT16);
        return l;
    }
}
