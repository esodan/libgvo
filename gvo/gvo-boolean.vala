/* gvls-boolean.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A object representing a boolean value usable when you want
 * to avoid a serialization to allwas set a boolean, instead
 * you can use this object to set an object's property to null
 * so the output will ignore this value no property in JSON or
 * {@link GLib.Variant} will be serialized.
 *
 * {{{
 *
 * class MyClass: GLib.Object, GVo.Object
 * {
 *     // Provide the output property's name on JSON or Variant using nick
 *     [Description (nick="IsEnable")]
 *     public GVo.Boolean is_enable { get; set; }
 *
 *     construct {
 *         // This sets 'is_enable' so the output will allways serialize out
 *         // this property, if you don't wnat so, just set to null
 *         is_enable = new GVo.Boolean ();
 *         is_enable.val = false;
 *     }
 * }
 *
 * public class App {
 *     private int main () {
 *         var myobj = new MyClass ();
 *         // This makes 'is_enable' property to be ignored at serialization
 *         // to Variant
 *         myobj.is_enable = null;
 *         return 0;
 *     }
 * }
 * }}}
 */
public class GVo.Boolean : GLib.Object, GVo.Object
{
  public bool val { get; set; }

  public GLib.Variant to_variant () {
    var v = new GLib.Variant.boolean (val);
    return v;
  }
  public void parse_variant (GLib.Variant v)
    requires (v.is_of_type (GLib.VariantType.BOOLEAN))
  {
    val = v.get_boolean ();
  }

    internal bool is_parseable (GLib.Variant v) {
        return v.is_of_type (GLib.VariantType.BOOLEAN);
    }

    internal GLib.List<GLib.VariantType> variant_types_parseable () {
        GLib.List<GLib.VariantType> l = new GLib.List<GLib.VariantType> ();
        l.append (GLib.VariantType.BOOLEAN);
        return l;
    }
}

