/* gvls-string.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A string object to be used in {@link Container} objects
 * to provide a list of strings when serialize to {@link GLib.Variant}
 * and JSON.
 */
public class GVo.String : GLib.Object, GVo.Object
{
    [Version (since="0.16")]
    public string @value { get; set; }

    construct {
        @value = "";
    }

    public String.for_string (string str) {
        @value = str;
    }

    public string to_string () { return @value; }

    public GLib.Variant to_variant () {
        return new GLib.Variant.string (@value);
    }

    public void parse_variant (GLib.Variant v)
        requires (v.is_of_type (GLib.VariantType.STRING))
    {
        @value = v.get_string ();
    }

    internal bool is_parseable (GLib.Variant v) {
        return v.is_of_type (GLib.VariantType.STRING);
    }

    internal GLib.List<GLib.VariantType> variant_types_parseable () {
        GLib.List<GLib.VariantType> l = new GLib.List<GLib.VariantType> ();
        l.append (GLib.VariantType.STRING);
        return l;
    }
}

/**
 * A string object to be used in {@link Container} objects,
 * not allowing to have multiple items of the same string
 * in the container
 */
public class GVo.HashableString : GVo.String, GVo.ContainerHashable {
  public HashableString.for_string (string str) {
    @value = str;
  }

  public uint hash () {
      return @value.hash ();
  }
  public bool equal (GLib.Object obj) {
      if (!(obj is GVo.String)) {
          return false;
      }
      var o = obj as GVo.String;
      return o.@value == @value;
  }
}
