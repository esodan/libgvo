/* gvls-named-string.vala
 *
 * Copyright 2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GLib;

/**
 * Named string values items
 */
public class GVo.NamedString : GLib.Object,
                        GVo.Object
{
    /**
     * Item's name
     */
    [Description (nick="name")]
    public string name { get; set; }
    /**
     * Item's value
     */
    [Description (nick="value")]
    public string @value { get; set; }

    public NamedString.from_values (string name, string @value) {
        this.name = name;
        this.@value = @value;
    }

    internal bool is_parseable (GLib.Variant v) {
        if (!v.is_container ()) {
            return false;
        }

        if (v.check_format_string ("a{sv}", false)) {
            var i = v.iterator ();
            string? k = null;
            GLib.Variant? val = null;
            if (i.next ("{sv}", out k, out val)) {
                if (!val.is_of_type (GLib.VariantType.STRING)) {
                    return false;
                }
            }

            return true;
        }

        return false;
    }
}

/**
 * Hashable named string values items
 * where name is the key
 */
public class GVo.HashableNamedString : GVo.NamedString,
                        GVo.ContainerHashable
{

    public HashableNamedString.from_values (string name, string @value) {
        this.name = name;
        this.@value = @value;
    }

    // GVls.ContainerHashable

    public uint hash () { return GLib.str_hash (name); }
    public bool equal (GLib.Object obj) {
        if (!(obj is HashableNamedString)) {
            return false;
        }

        return name == ((GVo.HashableNamedString) obj).name;
    }
}
