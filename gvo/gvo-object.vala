/* gvls-object-variant.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Implementators can be serialized to/from {@link GLib.Variant}
 *
 * Check at {@link to_variant} for details.
 */
public interface GVo.Object : GLib.Object
{
    /**
     * The object is packed as a {@link GLib.Variant} dictionary in the form 'a{sv}'.
     *
     * The object's properties are scanned and taken its nickname, as the
     * key of the Variant dictionary, allowing to use any name format.
     *
     * If the property is a {@link GLib.Object} but its value is a
     * {@link GVo.Object}, it is serialized as a {@link GLib.Variant}
     * with its nick name as the node's name.
     */
    public virtual GLib.Variant to_variant () {
        var b = new GLib.VariantBuilder (new GLib.VariantType ("a{sv}"));
        GLib.Value v;

        foreach (GLib.ParamSpec spec in this.get_class ().list_properties ()) { // FIXME: Search nick in implemented interfaces
            string nick = spec.get_nick ();
            if (nick == null || nick == "") {
                continue;
            }

            GLib.Type type = spec.value_type;
            if (type.is_a (typeof (string))) {
                v = GLib.Value (typeof (string));
                get_property (spec.name, ref v);
                string sv = (string) v;
                if (sv != null) {
                    b.add ("{sv}", nick, new GLib.Variant.string (sv != null ? sv : ""));
                }
            } else if (type.is_a (typeof (bool))) {
                v = GLib.Value (typeof (bool));
                get_property (spec.name, ref v);
                bool bv = (bool) v;
                b.add ("{sv}", nick, new GLib.Variant.boolean (bv));

            } else if (type.is_a (typeof (int))) {
                v = GLib.Value (typeof (int));
                get_property (spec.name, ref v);
                int iv = (int) v;
                b.add ("{sv}", nick, new GLib.Variant.int64 (iv));

            } else if (type.is_a (typeof (int64))) {
                v = GLib.Value (typeof (int64));
                get_property (spec.name, ref v);
                int64 iv = (int64) v;
                b.add ("{sv}", nick, new GLib.Variant.int64 (iv));

            } else if (type.is_a (GLib.Type.ENUM)) {
                v = GLib.Value (GLib.Type.ENUM);
                get_property (spec.name, ref v);
                int bv = v.get_enum ();
                b.add ("{sv}", spec.name, new GLib.Variant.int64 (bv));
            } else if (type.is_a (typeof (GVo.Object))) {
                v = GLib.Value (typeof (GVo.Object));
                get_property (spec.name, ref v);
                GVo.Object vv = (GLib.Object) v as GVo.Object;
                if (vv != null) {
                  b.add ("{sv}", nick, vv.to_variant ());
                }
            } else if (type.is_a (typeof (GLib.Object))) {
                v = GLib.Value (typeof (GLib.Object));
                get_property (spec.name, ref v);
                GLib.Object vv = (GLib.Object) v;
                if (vv != null) {
                    if (vv is GVo.Object) {
                        var vvo = vv as GVo.Object;
                        b.add ("{sv}", nick, vvo.to_variant ());
                    }
                }
            } else if (type.is_a (typeof (GLib.Variant))) {
                v = GLib.Value (typeof (GLib.Variant));
                get_property (spec.name, ref v);
                GLib.Variant vv = (GLib.Variant) v;
                if (vv != null) {
                    b.add ("{sv}", nick, vv);
                }
            } else if (type.is_a (typeof (GLib.Date))) {
                v = GLib.Value (typeof (GLib.Date));
                get_property (spec.name, ref v);
                GLib.Date d = (GLib.Date) v;
                if (d.valid ()) {
                    string s = "%d-%d-%d".printf (d.get_year (), d.get_month (), d.get_day ());
                    GLib.Variant vd = new GLib.Variant.string (s);
                    b.add ("{sv}", nick, vd);
                }
            } else if (type.is_a (typeof (GLib.DateTime))) {
                v = GLib.Value (typeof (GLib.DateTime));
                get_property (spec.name, ref v);
                GLib.DateTime d = (GLib.DateTime) v;
                if (d != null) {
                    string s = d.format_iso8601 ();
                    GLib.Variant vd = new GLib.Variant.string (s);
                    b.add ("{sv}", nick, vd);
                }
            }
        }

        return b.end ();
    }
    /**
     * The object is unpacked as a {@link GLib.Variant} dictionary in the form 'a{sv}'.
     *
     * The object's properties are scanned and taken its nickname, as the
     * key of the Variant dictionary, allowing to use any name format.
     *
     * If the property is a {@link GLib.Object} and its value is a
     * {@link GVo.Object}, it is deserialized to.
     */
    public virtual void parse_variant (GLib.Variant v)
    {
        GLib.Value val;
        var dict = new GLib.VariantDict(v);

        foreach (GLib.ParamSpec spec in this.get_class ().list_properties ()) {
            string nick = spec.get_nick ();
            if (nick == "") {
                continue;
            }

            GLib.Type type = spec.value_type;

            if (type.is_a (typeof (string))) {
                GLib.Variant vsv = dict.lookup_value (nick, GLib.VariantType.STRING);
                if (vsv != null && GLib.ParamFlags.WRITABLE in spec.flags) {
                    string sv = vsv.get_string ();
                    val = GLib.Value (typeof (string));
                    val = sv;
                    set_property (spec.name, val);
                }
            } else if (type.is_a (typeof (bool))) {
                GLib.Variant vbv = dict.lookup_value (nick, GLib.VariantType.BOOLEAN);
                if (vbv != null && GLib.ParamFlags.WRITABLE in spec.flags) {
                    bool bv = vbv.get_boolean ();
                    val = GLib.Value (typeof (bool));
                    val = bv;
                    set_property (spec.name, val);
                }
            } else if (type.is_a (typeof (int))) {
                GLib.Variant viv = dict.lookup_value (nick, GLib.VariantType.INT64);
                if (viv != null && GLib.ParamFlags.WRITABLE in spec.flags) {
                    int iv = (int) viv.get_int64 ();
                    val = GLib.Value (typeof (int));
                    val = iv;
                    set_property (spec.name, val);
                }
            } else if (type.is_a (typeof (int64))) {
                GLib.Variant viv = dict.lookup_value (nick, GLib.VariantType.INT64);
                if (viv != null && GLib.ParamFlags.WRITABLE in spec.flags) {
                    int64 iv = (int64) viv.get_int64 ();
                    val = GLib.Value (typeof (int64));
                    val = iv;
                    set_property (spec.name, val);
                }
                } else if (type.is_a (typeof (Container))) {
                GLib.Variant vv = dict.lookup_value (nick, GLib.VariantType.ARRAY);
                if (vv != null) {
                    GLib.Value cv = GLib.Value (typeof (GLib.Object));
                    get_property (spec.name, ref cv);
                    Container c = (GLib.Object) cv as Container;
                    if (c != null) {
                        c.parse_variant (vv);
                    } else {
                        debug ("Container property '%s' was not set in the object", spec.name);
                    }
                }
            } else if (type.is_a (GLib.Type.ENUM)) {
                GLib.Variant vvi = dict.lookup_value (nick, GLib.VariantType.INT64);
                if (vvi != null && GLib.ParamFlags.WRITABLE in spec.flags) {
                    int64 vi = vvi.get_int64 ();
                    GLib.Value cv = GLib.Value (GLib.Type.ENUM);
                    cv.set_enum ((int) vi);
                    set_property (spec.name, cv);
                }

            } else if (type.is_a (typeof (GVo.Object))) {
                GLib.List<GLib.VariantType> list = new GLib.List<GLib.VariantType> ();
                if (type.is_instantiatable ()) {
                    var no = GLib.Object.new (type);
                    list = ((GVo.Object) no).variant_types_parseable ();
                } else {
                    list = generic_variant_types_parseable ();
                }

                GLib.Variant vv = null;
                foreach (GLib.VariantType vtype in list) {
                    vv = dict.lookup_value (nick, vtype);
                    if (vv != null) {
                        break;
                    }
                }

                if (vv != null) {
                    GLib.Value cv = GLib.Value (typeof (GLib.Object));
                    get_property (spec.name, ref cv);
                    GLib.Object c = (GLib.Object) cv;
                    if (c == null
                        && GLib.ParamFlags.WRITABLE in spec.flags)
                    {
                        if (type.is_instantiatable ()) {
                            c = GLib.Object.new (type);
                            cv = c;
                            set_property (spec.name, cv);
                        } else {
                            c = generics_create_from_variant (vv);
                            cv = c;
                            set_property (spec.name, cv);
                        }
                    }

                    if (c != null) {
                        GVo.Object oc = (GVo.Object) c;
                        if (oc.is_parseable (vv)) {
                            oc.parse_variant (vv);
                        }
                    }
                }
            } else if (type.is_a (typeof (GLib.Object))) {
                val = GLib.Value (typeof (GLib.Object));
                get_property (spec.name, ref val);
                GLib.Object vo = (GLib.Object) val;
                if (vo != null) {
                    if (vo is GVo.Object) {
                        var vvo = vo as GVo.Object;
                        GLib.Variant vv = dict.lookup_value (nick, GLib.VariantType.DICTIONARY);
                        if (vv != null) {
                            vvo.parse_variant (vv);
                        }
                    }
                }
            } else if (type.is_a (typeof (GLib.Variant))) {
                GLib.Variant vv = dict.lookup_value (nick, GLib.VariantType.DICTIONARY);
                if (vv != null && GLib.ParamFlags.WRITABLE in spec.flags)
                {
                    GLib.Value cv = GLib.Value (GLib.Type.VARIANT);
                    cv.set_variant (vv);
                    set_property (spec.name, cv);
                }
            } else if (type.is_a (typeof (GLib.Date))) {
                GLib.Variant vv = dict.lookup_value (nick, GLib.VariantType.STRING);
                if (vv != null && GLib.ParamFlags.WRITABLE in spec.flags)
                {
                    string s = vv.get_string ();
                    if (s != null && s != "") {
                        GLib.Date d = GLib.Date ();
                        d.set_parse (s);
                        set_property (spec.name, d);
                    }
                }
            } else if (type.is_a (typeof (GLib.DateTime))) {
                GLib.Variant vv = dict.lookup_value (nick, GLib.VariantType.STRING);
                if (vv != null && GLib.ParamFlags.WRITABLE in spec.flags)
                {
                    string s = vv.get_string ();
                    if (s != null && s != "") {
                        GLib.DateTime d = new GLib.DateTime.from_iso8601 (s, new GLib.TimeZone.utc ());
                        if (d != null) {
                            set_property (spec.name, d);
                        }
                    }
                }
            }
        }
    }

    /**
     * Check if the {@link GLib.Variant} can be parsed by this object.
     *
     * By default all {@link GVo.Object} can parse {@link GLib.Variant}
     * objects with the format string 'a{sv}'
     */
    [Version (since="2.0")]
    public virtual bool is_parseable (GLib.Variant v) {
        if (!v.is_container ()) {
            return false;
        }

        if (!v.check_format_string ("a{sv}", false)) {
            return false;
        }

        return true;
    }

    /**
     * Provides a list of {@link GLib.VariantType} types suitable to be
     * parsed by this object.
     *
     * By default this object can parse {@link GLib.VariantType.DICTIONARY}
     * types
     */
    [Version (since="2.0")]
    public virtual GLib.List<GLib.VariantType> variant_types_parseable () {
        GLib.List<GLib.VariantType> l = new GLib.List<GLib.VariantType> ();
        l.append (GLib.VariantType.DICTIONARY);
        return l;
    }

    /**
     * Provides a list of {@link GLib.VariantType} types suitable to be
     * parsed by this object's properties defined as generic {@link GVo.Object}
     *
     * By default the types are the same as {@link GVo.Object} y.generics_variant_types_supported}
     */
    [Version (since="2.0")]
    public virtual GLib.List<GLib.VariantType> generic_variant_types_parseable () {
        return generics_variant_types_supported ();
    }

    /**
     * Introspect given {@link GLib.Variant} and
     * creates a {@link GVo.Object} derived class instance
     *
     * If this object has a {@link GVo.Object} property when
     * parsing a {@link GLib.Variant}, the parser use this
     * method to instantiate an object that is suitable to parse
     * the given {@link GLib.Variant}.
     *
     * Implementers should introspect the {@link GLib.Variant}
     * in order to detect the {@link GVo.Object} that can
     * parse it other than the default ones. If the default objects
     * for specific types is required, additionally of the custom ones,
     * the implementers can call {@link GVo.Object.create_from_variant}
     * after all its custome checks.
     */
    [Version (since="2.0")]
    public virtual GVo.Object? generics_create_from_variant (GLib.Variant v)
    {
        return create_from_variant (v);
    }

    // Static Methods

    /**
     * Creates an instance of a {@link GVo.Object} that have
     * parsed given {@link GLib.Variant}
     */
    [Version (since="2.0")]
    public static GVo.Object? create_from_variant (GLib.Variant v) {
        GLib.Type type = GVo.Object.suggest_parser (v);
        if (type.is_instantiatable () && type.is_a (typeof (GVo.Object))) {
            var o = GLib.Object.new (type) as GVo.Object;
            o.parse_variant (v);
            return o;
        }

        return null;
    }
    /**
     * Suggest a {@link GLib.Type} suitable to parse given
     * {@link GLib.Variant}
     */
    [Version (since="2.0")]
    public static GLib.Type suggest_parser (GLib.Variant v) {
        if (v.is_of_type (GLib.VariantType.STRING)) {
            return typeof (GVo.String);
        } else if (v.is_of_type (GLib.VariantType.INT64)) {
            return typeof (GVo.Integer);
        } else if (v.is_of_type (GLib.VariantType.INT32)) {
            return typeof (GVo.Integer);
        } else if (v.is_of_type (GLib.VariantType.INT16)) {
            return typeof (GVo.Integer);
        } else if (v.is_of_type (GLib.VariantType.BOOLEAN)) {
            return typeof (GVo.Boolean);
        } else if (v.is_of_type (GLib.VariantType.DICTIONARY)) {
            return typeof (GVo.MapString);
        }

        return GLib.Type.INVALID;
    }

    /**
     * Check if the given type can parse a given {@link GLib.Variant}
     */
    [Version (since="2.0")]
    public static bool can_be_parsed_by (GLib.Variant v, GLib.Type type) {
        debug ("Variant format string: %s", v.get_type_string ());
        if (type.is_a (typeof (GVo.String))) {
            return v.is_of_type (GLib.VariantType.STRING);
        } else if (type.is_a (typeof (GVo.Boolean))) {
            return v.is_of_type (GLib.VariantType.BOOLEAN);
        } else if (type.is_a (typeof (GVo.Integer))) {
            return v.is_of_type (GLib.VariantType.INT64);
        } else if (type.is_a (typeof (GVo.Int64))) {
            return v.is_of_type (GLib.VariantType.INT64);
        } else if (type.is_a (typeof (GVo.Int32))) {
            return v.is_of_type (GLib.VariantType.INT32);
        } else if (type.is_a (typeof (GVo.Int16))) {
            return v.is_of_type (GLib.VariantType.INT16);
        } else if (type.is_a (typeof (GVo.Object))) {
            if (!v.is_container ()) {
                return false;
            }

            if (!v.check_format_string ("a{sv}", false)) {
                return false;
            }
        }

        if (type.is_a (typeof (GVo.Object))) {
            var o = GLib.Object.new (type) as GVo.Object;
            return o.is_parseable (v);
        }

        return true;
    }

    /**
     * Check if the given {@link GLib.Variant} can be parsed to
     * given {@link GLib.Type}
     */
    [Version (since="2.0")]
    public static bool can_be_parsed_to (GLib.Variant v, GLib.Type type) {
        return false;
    }

    /**
     * Generates a list of {@link GLib.VariantType} that can be
     * parsed by this library.
     */
    [Version (since="2.0")]
    public static GLib.List<GLib.VariantType> generics_variant_types_supported () {
        GLib.List<GLib.VariantType> l = new GLib.List<GLib.VariantType> ();
        l.append (GLib.VariantType.DICTIONARY);
        l.append (GLib.VariantType.STRING);
        l.append (GLib.VariantType.BOOLEAN);
        l.append (GLib.VariantType.INT64);
        l.append (GLib.VariantType.INT32);
        l.append (GLib.VariantType.INT16);
        return l;
    }
}
