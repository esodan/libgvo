/* gvls-named.vala
 *
 * Copyright 2020 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * A named {@link GVo.Object}. It can be converted to
 * a {@link GLib.Variant} of type {@link GLib.VariantType.DICT_ENTRY}
 */
[Version (since="2.0")]
public class GVo.Named : GLib.Object,
                        GVo.ContainerHashable,
                        GVo.Object
{
    private Gee.ArrayList<GLib.Type> types = new Gee.ArrayList<GLib.Type> ();
    private GVo.Object _object = null;
    /**
     * Entry name as key of the dictionary
     */
    public string name { get; set; }

    /**
     * An object as the value of the dictionary entry of type '{sv}'
     */
    public GVo.Object object {
        get {
            return _object;
        }
        set {
            _object = value;
            if (types.size != 0) {
                bool found = false;
                foreach (GLib.Type t in types) {
                    if (_object.get_type ().name () == t.name ()) {
                        found = true;
                        break;
                    }
                }

                if (!found) {
                    warning (_("This object is not declared as parseable in this class"));
                }
            }
        }
    }

    public Named.with_name (string name) {
        _name = name;
    }

    public Named.with_object (string name, GVo.Object obj) {
        _name = name;
        object = obj;
    }

    /**
     * Add supported types for {@link object} to parse
     * variant objects to
     */
    public void add_parseable_type (GLib.Type type) {
        types.add (type);
    }
    /**
     * Provides supported types for {@link object} to parse
     * variant objects to
     */
    public GLib.List<GLib.Type> get_parseable_types () {
        GLib.List<GLib.Type> l = new GLib.List<GLib.Type> ();
        foreach (GLib.Type t in types) {
            l.append (t);
        }

        return l;
    }

    /**
     * Create instance based on given name
     */


    internal GLib.Variant to_variant () {
        var b = new GLib.VariantBuilder (new GLib.VariantType ("a{sv}"));
        string str = name == null || name == "" ? _("No Name") : name;
        if (object != null) {
            b.add ("{sv}", str, object.to_variant ());
        } else {
            b.add ("{sv}", str, new GLib.Variant.string (str));
        }

        return b.end ();
    }

    internal void parse_variant (GLib.Variant v)
    {
        if (object != null) {
            object.parse_variant (v);
        }
    }

    internal uint hash () {
        return GLib.str_hash (name);
    }

    internal bool equal (GLib.Object obj) {
        if (!(obj is Named)) {
            return false;
        }

        var o = obj as Named;
        bool is_equal = true;
        if (object is GVo.ContainerHashable) {
            is_equal = ((GVo.ContainerHashable) object).equal (o.object);
        }

        return o.name == name && is_equal;
    }

    internal bool is_parseable (GLib.Variant v) {
        if (object != null) {
            return object.is_parseable (v);
        }

        if (object == null && types.size != 0) {
            foreach (GLib.Type t in types) {
                var o = GLib.Object.new (t);
                if (o is GVo.Object) {
                    if (((GVo.Object) object).is_parseable (v)) {
                        object = (GVo.Object) o;
                        break;
                    }
                }
            }
        }

        return true;
    }
}
