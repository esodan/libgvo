/* gvls-duration.vala
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A object representing a duration according to
 * ISO 8601 duration, e.g. “P1DT18H”
 */
[Version (since="2.0")]
public class GVo.DateTime : GLib.Object, GVo.Object
{
    public GLib.DateTime val { get; set; }

    public string to_string () {
        if (val == null) {
            return "";
        }

        return val.format_iso8601 ();
    }

    internal GLib.Variant to_variant () {
        GLib.Variant v = new GLib.Variant.maybe (GLib.VariantType.STRING, null);
        string s = null;
        if (val != null) {
            s = val.format_iso8601 ();
            v = new GLib.Variant.string (s);
        }

        return v;
    }

    internal void parse_variant (GLib.Variant v)
    {
        if (!v.is_of_type (GLib.VariantType.STRING)) {
            return;
        }

        string s = v.get_string ();
        val = new GLib.DateTime.from_iso8601 (s, new GLib.TimeZone.utc ());
    }

    internal bool is_parseable (GLib.Variant v) {
        if (v.is_of_type (GLib.VariantType.STRING)) {
            return true;
        } else if (v.is_of_type (GLib.VariantType.MAYBE)) {
            return true;
        }

        return false;
    }

    internal GLib.List<GLib.VariantType> variant_types_parseable () {
        GLib.List<GLib.VariantType> l = new GLib.List<GLib.VariantType> ();
        l.append (GLib.VariantType.STRING);
        l.append (GLib.VariantType.MAYBE);
        return l;
    }
}

