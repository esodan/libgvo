/* gvo-map-string.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General internal License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General internal License for more details.
 *
 * You should have received a copy of the GNU Lesser General internal License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

[Version (since="2.0")]
public errordomain GVo.MapStringError {
    INVALID_NAME_ERROR,
    INVALID_TYPE_ERROR
}

/**
 * A container of type 'a{sv}' as a dictinonary of {@link GLib.Variant}
 * objects, representing a dictionary of JSON objects with a property string
 * as a key.
 *
 * When serializing to {@link GLib.Variant} properties keys are not warrantied
 * to be in the same order they were added to the container, because it the keys
 * are stored in a hashed map.
 */
[Version (since="2.0")]
public class GVo.MapString : GLib.Object, GVo.Object
{
    Gee.HashMap<string,GLib.Variant> map = new Gee.HashMap<string,GLib.Variant> ();

    /**
     * Create from a {@link GVo.Container} but only if
     * its members are {@link GVo.Named} objects
     */
    public static GVo.MapString new_from_container (GVo.Container container)
    {
        GVo.MapString m = new GVo.MapString ();
        for (int i = 0; i < container.get_n_items (); i++) {
            GVo.Named o = container.get_item (i) as GVo.Named;
            if (o == null) {
                continue;
            }

            if (o.object == null) {
                continue;
            }

            m.add (o.name, o.object.to_variant ());
        }

        return m;
    }

    /**
     * Add a new {@link GLib.Variant} representing a JSON
     * object associated to a given name
     */
    public void add (string name, GLib.Variant v) {
        map.set (name, v);
    }

    /**
     * Removes a {@link GLib.Variant} with the given name
     */
    public void remove (string name) {
        map.unset (name);
    }

    /**
     * Get the {@link GLib.Variant} with the given name
     * in the collection.
     */
    public GLib.Variant? get_variant (string name) {
        return map.get (name);
    }

    /**
     * Parse a named {@link GLib.Variant} in the collection
     * to a given type of {@link GVo.Object}
     *
     * If the given type is not a {@link GVo.Object} type
     * the a warning is rised and returns a {@link GVo.String}
     * with the given name, even if the object with that name
     * doesn't exists.
     */
    public GVo.Object? parse_to (string name, GLib.Type type) throws GLib.Error
    {
        if (!type.is_a (typeof (GVo.Object))) {
            throw new GVo.MapStringError.INVALID_TYPE_ERROR (_("Unable to parse GLib.Variant to type: %s"), type.name ());
        }

        GVo.Object obj = null;
        if (type.is_a (typeof (GVo.Named))) {
            obj = GLib.Object.new (type, name: name) as GVo.Object;
        } else {
            obj = GLib.Object.new (type) as GVo.Object;
        }

        GLib.Variant v = map.get (name);
        if (v == null) {
            throw new GVo.MapStringError.INVALID_NAME_ERROR (_("No GLib.Variant object found with name: %s"), name);
        }

        obj.parse_variant (v);
        return obj;
    }

    /**
     * Introspect {@link GLib.Variant} value in the container
     * for the given property's name in order to find if it is
     * compatible with the given object instance
     */
    public bool is_property_compatible_with (string name, GLib.Object object) {
        GLib.Variant v = map.get (name);
        if (v == null) {
            return false;
        }

        if (object is GVo.Object) {
            return ((GVo.Object) object).is_parseable (v);
        }

        if (object.get_type ().is_a (GVo.Object.suggest_parser (v))) {
            return true;
        }

        return false;
    }

    // GVo.Object
    internal GLib.Variant to_variant () {
        var b = new GLib.VariantBuilder (new GLib.VariantType ("a{sv}"));
        foreach (string k in map.keys) {
            var v = map.get (k);
            b.add ("{sv}", k, v);
        }

        return b.end ();
    }

    internal void parse_variant (GLib.Variant v) {
        if (!v.is_of_type (GLib.VariantType.DICTIONARY)) {
            return;
        }

        GLib.Variant? val = null;
        string? key = null;
        GLib.VariantIter iter = v.iterator ();

        while (iter.next ("{sv}", out key, out val)) {
            map.set (key, val);
        }
    }
}
