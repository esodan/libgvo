option('docs', type: 'boolean', value : 'true', description : 'Disable Vala Documentation')
option('introspection', type: 'boolean', value : 'true', description : 'Build GObject Introspection bindings genereation')
option('subprojects', type: 'boolean', value : 'false', description : 'Use subprojects for not found dependencies') 
