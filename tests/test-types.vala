/*
 * GVo Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2018 <esodan@gmail.com>
 *
 * GVo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GVls is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Tests.UnitTest {
    static int main (string[] args)
    {
        GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
        GLib.Test.init (ref args);
        GLib.Test.add_func ("/gvo/types/date",
        ()=>{
            var n = new TypesDate ();
            var v = n.to_variant ();
            message ("Output:\n%s", v.print (true));
            var dt = new GLib.DateTime.now ();
            message ("Now time: %s", dt.to_string ());
            var d = GLib.Date ();
            d.set_dmy ((GLib.DateDay) dt.get_day_of_month (), dt.get_month (), (GLib.DateYear) dt.get_year ());
            assert (d.valid ());
            n.date = d;
            message ("%d-%d-%d".printf (n.date.get_year (), n.date.get_month (), n.date.get_day ()));
            assert (n.date.valid ());
            var v1 = n.to_variant ();
            message ("Output:\n%s", v1.print (true));

            var n1 = new TypesDate ();
            assert (!n1.date.valid ());
            n1.parse_variant (v1);
            assert (n1.date.valid ());
            var v2 = n1.to_variant ();
            message ("Output:\n%s", v2.print (true));
        });
        GLib.Test.add_func ("/gvo/types/datetime",
        ()=>{
            var n = new TypesDate ();
            var v = n.to_variant ();
            message ("Output:\n%s", v.print (true));
            n.timestamp = new GLib.DateTime.now ();
            message ("Now time: %s", n.timestamp.to_string ());
            var v1 = n.to_variant ();
            message ("Output:\n%s", v1.print (true));

            var n1 = new TypesDate ();
            n1.parse_variant (v1);
            var v2 = n1.to_variant ();
            message ("Output:\n%s", v2.print (true));
        });
        GLib.Test.add_func ("/gvo/types/duration",
        ()=>{
            var d1 = Date ();
            d1.set_dmy ((DateDay) 1, (DateMonth) 1, (DateYear) 2022);
            var d2 = Date ();
            d2.set_dmy ((DateDay) 1, (DateMonth) 1, (DateYear) 2022);
            d2.add_days (1);

            var d = new GVo.Duration ();
            d.dates_duration (d1, d2);
            message ("Duration: %s", d.val);
            assert (d.val == "P0Y0M1DT0H0M0S");

            int years = 0;
            int months = 0;
            int days = 0;
            int hours = 0;
            int minutes = 0;
            int seconds = 0;
            d.get_duration_parts (out years,
                                  out months,
                                  out days,
                                  out hours,
                                  out minutes,
                                  out seconds);
            assert (years == 0);
            assert (months == 0);
            assert (days == 1);
            assert (hours == 0);
            assert (minutes == 0);
            assert (seconds == 0);

            d2.set_dmy ((DateDay) 1, (DateMonth) 1, (DateYear) 2022);
            d2.add_days (31);
            d.dates_duration (d1, d2);
            message ("Duration: %s", d.val);
            assert (d.val == "P0Y1M0DT0H0M0S");
            d.get_duration_parts (out years,
                                  out months,
                                  out days,
                                  out hours,
                                  out minutes,
                                  out seconds);
            assert (years == 0);
            assert (months == 1);
            assert (days == 0);
            assert (hours == 0);
            assert (minutes == 0);
            assert (seconds == 0);

            d2.set_dmy ((DateDay) 1, (DateMonth) 1, (DateYear) 2022);
            d2.add_days (32);
            d.dates_duration (d1, d2);
            message ("Duration: %s", d.val);
            assert (d.val == "P0Y1M1DT0H0M0S");
            d.get_duration_parts (out years,
                                  out months,
                                  out days,
                                  out hours,
                                  out minutes,
                                  out seconds);
            assert (years == 0);
            assert (months == 1);
            assert (days == 1);
            assert (hours == 0);
            assert (minutes == 0);
            assert (seconds == 0);

            d2.set_dmy ((DateDay) 1, (DateMonth) 1, (DateYear) 2022);
            d2.add_days (365);
            d.dates_duration (d1, d2);
            message ("Duration: %s", d.val);
            assert (d.val == "P1Y0M0DT0H0M0S");
            d.get_duration_parts (out years,
                                  out months,
                                  out days,
                                  out hours,
                                  out minutes,
                                  out seconds);
            assert (years == 1);
            assert (months == 0);
            assert (days == 0);
            assert (hours == 0);
            assert (minutes == 0);
            assert (seconds == 0);

            d2.set_dmy ((DateDay) 1, (DateMonth) 1, (DateYear) 2022);
            d2.add_days (397);
            d.dates_duration (d1, d2);
            message ("Duration: %s", d.val);
            assert (d.val == "P1Y1M1DT0H0M0S");
            d.get_duration_parts (out years,
                                  out months,
                                  out days,
                                  out hours,
                                  out minutes,
                                  out seconds);
            assert (years == 1);
            assert (months == 1);
            assert (days == 1);
            assert (hours == 0);
            assert (minutes == 0);
            assert (seconds == 0);

            GLib.DateTime t1 = new GLib.DateTime (new TimeZone.utc (), 2022, 1, 1, 0, 0, 0.0);
            GLib.DateTime t2 = t1.add_seconds (3);
            d.timestamps_duration (t1, t2);
            message ("Duration: %s", d.val);
            assert (d.val == "P0Y0M0DT0H0M3S");
            d.get_duration_parts (out years,
                                  out months,
                                  out days,
                                  out hours,
                                  out minutes,
                                  out seconds);
            assert (years == 0);
            assert (months == 0);
            assert (days == 0);
            assert (hours == 0);
            assert (minutes == 0);
            assert (seconds == 3);

            t2 = t1.add_minutes (10);
            d.timestamps_duration (t1, t2);
            message ("Duration: %s", d.val);
            assert (d.val == "P0Y0M0DT0H10M0S");
            d.get_duration_parts (out years,
                                  out months,
                                  out days,
                                  out hours,
                                  out minutes,
                                  out seconds);
            assert (years == 0);
            assert (months == 0);
            assert (days == 0);
            assert (hours == 0);
            assert (minutes == 10);
            assert (seconds == 0);

            t2 = t1.add_hours (2);
            t2 = t2.add_minutes (7);
            d.timestamps_duration (t1, t2);
            message ("Duration: %s", d.val);
            assert (d.val == "P0Y0M0DT2H7M0S");
            d.get_duration_parts (out years,
                                  out months,
                                  out days,
                                  out hours,
                                  out minutes,
                                  out seconds);
            assert (years == 0);
            assert (months == 0);
            assert (days == 0);
            assert (hours == 2);
            assert (minutes == 7);
            assert (seconds == 0);

            t2 = t1.add_hours (11);
            t2 = t2.add_minutes (9);
            t2 = t2.add_seconds (34.837);
            d.timestamps_duration (t1, t2);
            message ("Duration: %s", d.val);
            assert (d.val == "P0Y0M0DT11H9M34S");
            d.get_duration_parts (out years,
                                  out months,
                                  out days,
                                  out hours,
                                  out minutes,
                                  out seconds);
            assert (years == 0);
            assert (months == 0);
            assert (days == 0);
            assert (hours == 11);
            assert (minutes == 9);
            assert (seconds == 34);

            var n = new TypesDate ();
            var v = n.to_variant ();
            message ("Output:\n%s", v.print (true));

            n.duration = d;
            var v1 = n.to_variant ();
            message ("Output:\n%s", v1.print (true));

            var n2 = new TypesDate ();
            n2.parse_variant (v1);
            var v2 = n2.to_variant ();
            message ("Output:\n%s", v2.print (true));
            assert (n2.duration != null);
            assert (n2.duration.val == n.duration.val);
        });
        GLib.Test.add_func ("/gvo/types/integer",
        ()=>{
            var n = new IntegerTester ();
            var v = n.to_variant ();
            message ("Output:\n%s", v.print (true));
            n.id = new GVo.Integer ();
            n.id.val = 820;
            var v1 = n.to_variant ();
            message ("Output:\n%s", v1.print (true));

            var n1 = new IntegerTester ();
            n1.parse_variant (v1);
            var v2 = n1.to_variant ();
            message ("Output:\n%s", v2.print (true));
            assert (n1.id.val == 820);

            var i = new GVo.Integer ();
            var i64 = new GVo.Int64 ();
            i64.val = 728;
            var i64v = i64.to_variant ();
            message ("Output:\n%s", i64v.print (true));
            i.parse_variant (i64v);
            assert (i.val == 728);

            var i32 = new GVo.Int32 ();
            i32.val = 34;
            var i32v = i32.to_variant ();
            message ("Output:\n%s", i32v.print (true));
            i.parse_variant (i32v);
            assert (i.val == 34);

            var i16 = new GVo.Int16 ();
            i16.val = 2788;
            var i16v = i16.to_variant ();
            message ("Output:\n%s", i16v.print (true));
            i.parse_variant (i16v);
            assert (i.val == 2788);
        });
        GLib.Test.add_func ("/gvo/types/generics",
        ()=>{
            var n = new Generics ();
            var v = n.to_variant ();
            message ("Output:\n%s", v.print (true));
            n.object = new GVo.Integer ();
            var v1 = n.to_variant ();
            message ("Output:\n%s", v1.print (true));

            var n1 = new Generics ();
            n1.parse_variant (v1);
            var v2 = n1.to_variant ();
            message ("Output:\n%s", v2.print (true));
            assert (n1.object != null);
            assert (n1.object is GVo.Integer);

            var n2 = new Generics ();
            n2.object = new GVo.String.for_string ("My Text");
            var v3 = n2.to_variant ();
            message ("Output:\n%s", v3.print (true));
            var n3 = new Generics ();
            n3.parse_variant (v3);
            var v4 = n3.to_variant ();
            message ("Output:\n%s", v4.print (true));
            assert (n3.object != null);
            assert (n3.object is GVo.String);

            var tdo = new Tests.TypesDate ();
            var dt = new GLib.DateTime.now ();
            message ("Now time: %s", dt.to_string ());
            var d = GLib.Date ();
            d.set_dmy ((GLib.DateDay) dt.get_day_of_month (), dt.get_month (), (GLib.DateYear) dt.get_year ());
            assert (d.valid ());
            tdo.date = d;
            var n4 = new Generics ();
            n4.object = tdo;
            var v5 = n4.to_variant ();
            message ("Output:\n%s", v5.print (true));
            var n5 = new Generics ();
            n5.parse_variant (v5);
            var v6 = n5.to_variant ();
            message ("Output:\n%s", v6.print (true));
            assert (n5.object != null);
            assert (n5.object is Tests.TypesDate);

            var n6 = new Generics ();
            n6.object = new Tests.PropertyObjectBool ();
            var v7 = n6.to_variant ();
            message ("Output:\n%s", v7.print (true));
            var n7 = new Generics ();
            n7.parse_variant (v7);
            var v8 = n7.to_variant ();
            message ("Output:\n%s", v8.print (true));
            assert (n7.object != null);
            assert (n7.object is Tests.PropertyObjectBool);
        });
        GLib.Test.add_func ("/gvo/types/generics/mapstring",
        ()=>{
            var n = new GenericsMap ();
            n.object = new Tests.PropertyObjectBool ();
            var v1 = n.to_variant ();
            message ("Output:\n%s", v1.print (true));


            var n2 = new GenericsMap ();
            n2.parse_variant (v1);
            var v2 = n2.to_variant ();
            message ("Output:\n%s", v2.print (true));
            assert (n2.object != null);
            assert (n2.object is GVo.MapString);
        });
        GLib.Test.add_func ("/gvo/types/date-time",
        ()=>{
            var o = new TypesDate ();
            var n = new GVo.DateTime ();
            o.datetime = n;
            var v1 = o.to_variant ();
            message ("Output:\n%s", v1.print (true));
            var v2 = n.to_variant ();
            message ("Output:\n%s", v2.print (true));


            var n2 = new GVo.DateTime ();
            n2.parse_variant (v2);
            var v3 = n2.to_variant ();
            message ("Output:\n%s", v3.print (true));
            assert (n2.val == null);


            var o2 = new TypesDate ();
            o2.parse_variant (v1);
            var v4 = o2.to_variant ();
            message ("Output:\n%s", v4.print (true));
            assert (o2.datetime != null);
            assert (o2.datetime.val == null);

            var o3 = new TypesDate ();
            assert (o3.datetime == null);
            var v5 = o3.to_variant ();
            message ("Output:\n%s", v5.print (true));

            var dt = new GLib.DateTime.now ();
            message ("Now time: %s", dt.to_string ());
            o3.datetime = new GVo.DateTime ();
            o3.datetime.val = dt;
            var v6 = o3.to_variant ();
            message ("Output:\n%s", v6.print (true));

            var o4 = new TypesDate ();
            o4.parse_variant (v6);
            var v7 = o4.to_variant ();
            message ("Output:\n%s", v7.print (true));
            assert (o4.datetime.to_string () == o3.datetime.to_string ());
        });
        GLib.Test.add_func ("/gvo/types/date-object",
        ()=>{
            var o = new TypesDate ();
            var n = new GVo.Date ();
            o.date_object = n;
            var v1 = o.to_variant ();
            message ("Output:\n%s", v1.print (true));
            var v2 = n.to_variant ();
            message ("Output:\n%s", v2.print (true));


            var n2 = new GVo.Date ();
            n2.parse_variant (v2);
            var v3 = n2.to_variant ();
            message ("Output:\n%s", v3.print (true));
            assert (!n2.val.valid ());


            var o2 = new TypesDate ();
            o2.parse_variant (v1);
            var v4 = o2.to_variant ();
            message ("Output:\n%s", v4.print (true));
            assert (o2.date_object != null);
            assert (!o2.date_object.val.valid ());

            var o3 = new TypesDate ();
            assert (o3.date_object == null);
            var v5 = o3.to_variant ();
            message ("Output:\n%s", v5.print (true));

            var dt = new GLib.DateTime.now ();
            message ("Now time: %s", dt.to_string ());
            GLib.Date d = GLib.Date ();
            d.set_dmy ((DateDay) dt.get_day_of_month (),
                    (DateMonth) dt.get_month (),
                    (DateYear) dt.get_year ());
            assert (d.valid ());
            o3.date_object = new GVo.Date ();
            o3.date_object.val = d;
            var v6 = o3.to_variant ();
            message ("Output:\n%s", v6.print (true));

            var o4 = new TypesDate ();
            o4.parse_variant (v6);
            var v7 = o4.to_variant ();
            message ("Output:\n%s", v7.print (true));
            message ("Dates: Origin: %s : Parsed: %s", o3.date_object.to_string (), o4.date_object.to_string ());
            assert (o4.date_object.to_string () == o3.date_object.to_string ());
        });

        return GLib.Test.run ();
    }
}
