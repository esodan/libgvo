/*
 * GVo Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2018 <esodan@gmail.com>
 *
 * GVo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GVls is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Tests.UnitTest {
    static int main (string[] args)
    {
        GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
        Test.init (ref args);
        Test.add_func ("/gvo/map-string/default",
        ()=>{
            var ms = new GVo.MapString ();
            var v = ms.to_variant ();
            message ("Output:\n%s", v.print (true));

            // Creating a dictinonary object with a list of dictionary objects
            var l = new Tests.ListObject ();
            var o1 = new PropertyObject ();
            o1.my_test = "Test1";
            var o2 = new PropertyObject ();
            o2.my_test = "Test2";
            l.my_list.add (o1);
            l.my_list.add (o2);
            GLib.Variant vo = l.to_variant ();

            // Adding an object to map with a named property
            ms.add ("Property1", vo);
            var v1 = ms.to_variant ();
            message ("Output:\n%s", v1.print (true));

            // This is a simple list
            var l2 = new GVo.ContainerHashList ();
            var o3 = new PropertyObject ();
            o3.my_test = "Test3";
            var o4 = new PropertyObject ();
            o4.my_test = "Test4";
            l2.add (o3);
            l2.add (o4);
            GLib.Variant vo2 = l2.to_variant ();

            // Adding a list to map with a named property
            ms.add ("ListProp1", vo2);
            var v2 = ms.to_variant ();
            message ("Output:\n%s", v2.print (true));

            // Adding a GVo.Object
            var po = new PropertyObject ();
            po.my_test = "PTest_1";
            var vpo = po.to_variant ();
            ms.add ("ObjectProperty", vpo);


            var v3a = ms.to_variant ();
            message ("Output:\n%s", v3a.print (true));

            // Parsing
            var ms2 = new GVo.MapString ();
            ms2.parse_variant (v3a);
            var v3 = ms2.to_variant ();
            message ("Output:\n%s", v3.print (true));

            var vp1 = ms2.get_variant ("Property1");
            assert (vp1 != null);
            try {
                var lco = ms2.parse_to ("Property1", typeof (ListObject));
                assert (lco != null);
                assert (lco is ListObject);
                var lo = (ListObject) lco;
                assert (lo.my_list != null);
                assert (lo.my_list.get_n_items () == 2);
                var li0 = lo.my_list.get_item (0);
                assert (li0 != null);
                assert (li0 is PropertyObject);
                var li1 = lo.my_list.get_item (1);
                assert (li1 != null);
                assert (li1 is PropertyObject);

                var p = ms2.parse_to ("ObjectProperty", typeof (PropertyObject));
                assert (p != null);
                assert (p is PropertyObject);
                var op = (PropertyObject) p;
                assert (op.my_test == "PTest_1");
            } catch (GLib.Error e) {
                warning ("Error: %s", e.message);
            }

            var ln = new GVo.ContainerHashList ();
            var liv1 = new GVo.String.for_string ("s1");
            var no1 = new GVo.Named ();
            no1.name = "P1";
            no1.object = liv1;
            ln.add (no1);
            var liv2 = new GVo.String.for_string ("s2");
            var no2 = new GVo.Named ();
            no2.name = "P2";
            no2.object = liv2;
            ln.add (no2);
            message ("List Output:\n%s", ln.to_variant ().print (true));

            var ms3 = GVo.MapString.new_from_container (ln);
            var msv3 = ms3.to_variant ();
            message ("Output:\n%s", msv3.print (true));
            assert (msv3.is_container ());
            assert (msv3.n_children () == 2);
            assert (msv3.lookup_value ("P1", GLib.VariantType.STRING) != null);
            assert (msv3.lookup_value ("P2", GLib.VariantType.STRING) != null);
        });

        return Test.run ();
    }
}
