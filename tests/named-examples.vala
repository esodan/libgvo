

public class Tests.PropertyObject : GLib.Object, GVo.Object
{
    [Description (nick="myTest")]
    public string my_test { get; set; default = "Some Value"; }
}

public class Tests.BaseProperty : GLib.Object, GVo.Object
{
    protected string _type = "_base";
    [Description (nick="_type")]
    public string object_type {
        get { return _type; }
        set {}
    }

    internal bool is_parseable (GLib.Variant v) {
        if (!v.is_container ()) {
            return false;
        }

        if (!v.check_format_string ("a{sv}", false)) {
            return false;
        }

        GLib.Variant vp = v.lookup_value ("_type", GLib.VariantType.STRING);
        if ( vp == null) {
            return false;
        }

        string n = vp.get_string ();
        if (n != object_type) {
            return false;
        }

        return true;
    }

}


public class Tests.PropertyObjectBool : Tests.BaseProperty
{
    [Description (nick="conditions")]
    public GVo.Container conditions { get; set; default = new GVo.ContainerHashList.for_type (typeof (GVo.Boolean)); }

    construct {
        _type = "PropertyObjectBool";
    }
}

public class Tests.PropertyObjectString : Tests.BaseProperty
{
    [Description (nick="names")]
    public GVo.Container names { get; set; default = new GVo.ContainerHashList.for_type (typeof (GVo.String)); }

    construct {
        _type = "PropertyObjectString";
    }
}

public class Tests.ListObject : GLib.Object, GVo.Object
{
    [Description (nick="myList")]
    public GVo.Container my_list { get; set; }

    construct {
        my_list = new GVo.ContainerHashList.for_type (typeof (PropertyObject));
    }
}

public class Tests.TypesDate : GLib.Object, GVo.Object
{
    [Description (nick="date")]
    public GLib.Date date { get; set; }

    [Description (nick="timestamp")]
    public GLib.DateTime timestamp { get; set; }

    [Description (nick="duration")]
    public GVo.Duration duration { get; set; }

    [Description (nick="datetime")]
    public GVo.DateTime datetime { get; set; }

    [Description (nick="dateObject")]
    public GVo.Date date_object { get; set; }
}

public class Tests.NamedContainer : GLib.Object, GVo.Object
{
    [Description (nick="collection")]
    public GVo.Container col { get; set; }

    construct {
        var c = new GVo.ContainerNamed ();
        c.add_parseable_type (typeof (GVo.String));
        c.add_parseable_type (typeof (GVo.Boolean));
        col = c;
    }
}

public class Tests.IntegerTester : GLib.Object, GVo.Object
{
    [Description (nick="id")]
    public GVo.Integer id { get; set; }
}

// Here you can see generics support in action
public class Tests.Generics : GLib.Object, GVo.Object
{
    [Description (nick="object")]
    public GVo.Object object { get; set; }

    // Here we introspect a given variant targeting 'object' property
    // to create an instance of Tests.TypesDate or Tests.PropertyObjectBool
    // depending on the properties in the dictionary
    internal GVo.Object? generics_create_from_variant (GLib.Variant v)
    {
        // Here we introspect the variant to find an specific type
        // and if it has a property with the required value in order
        // to identify the object type to create
        if (v.is_of_type (GLib.VariantType.DICTIONARY)) {
            // Checking if is a Tests.TypesDate
            var vop = v.lookup_value ("date", GLib.VariantType.STRING);
            if (vop != null) {
                GLib.Date d = GLib.Date ();
                d.set_parse (vop.get_string ());
                if (d.valid ()) {
                    var obj = new Tests.TypesDate ();
                    obj.parse_variant (v);
                    return obj;
                }
            }
            // Checking if is a Tests.PropertyObjectBool
            vop = v.lookup_value ("_type", GLib.VariantType.STRING);
            if (vop != null) {
                string vt = vop.get_string ();
                if (vt == "PropertyObjectBool") {
                    var obj = new Tests.PropertyObjectBool ();
                    obj.parse_variant (v);
                    return obj;
                }
            }
        }

        // No specific object is detected, but we want
        // to support any other supported by the library,
        // so use a generic creator and parser
        return GVo.Object.create_from_variant (v);
    }
}

// Here we declare a Generic object with no custom creator
// so only standard library objects are used depending on the
// Variant Type, but also will be used to test if a
// Variant Dictionary type can be parsed to a GVo.MapString
public class Tests.GenericsMap : GLib.Object, GVo.Object
{
    [Description (nick="object")]
    public GVo.Object object { get; set; }
}
