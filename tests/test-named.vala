/*
 * GVo Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2018 <esodan@gmail.com>
 *
 * GVo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GVls is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Tests.UnitTest {
    static int main (string[] args)
    {
        GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
        GLib.Test.init (ref args);
        GLib.Test.add_func ("/gvo/named/default",
        ()=>{
            var n = new GVo.Named ();
            n.name = "Property1";
            var v = n.to_variant ();
            message ("Output:\n%s", v.print (true));

            var o1 = new Tests.PropertyObject ();
            n.object = o1;
            var v1 = n.to_variant ();
            message ("Output:\n%s", v1.print (true));

            var n1 = new GVo.Named ();
            n1.name = "Property2";
            n1.parse_variant (v);
            var n1v = n1.to_variant ();
            message ("Output:\n%s", n1v.print (true));
            assert (n1.object == null);

            var n2 = new GVo.Named ();
            n2.name = "Property3";
            n2.parse_variant (v1);
            assert (n2.object == null);

            var o2 = new Tests.PropertyObject ();
            o2.my_test = "Changed Value 1";
            n2.object = o2;
            n2.parse_variant (v1);
            var n2v = n2.to_variant ();
            message ("Output:\n%s", n2v.print (true));
            assert (n2.object != null);
            assert (n2.object is Tests.PropertyObject);
            assert (((Tests.PropertyObject) n2.object).my_test == "Changed Value 1");
        });
        GLib.Test.add_func ("/gvo/named/optionals/fail",
        ()=>{
            var n = new Tests.NamedContainer ();
            var v = n.to_variant ();
            message ("Output:\n%s", v.print (true));
            var n1 = new GVo.Named ();
            n1.name = "P1";
            n1.object = new GVo.String.for_string ("ValueString");
            var v1 = n.to_variant ();
            message ("Output:\n%s", v1.print (true));

        });

        return GLib.Test.run ();
    }
}
