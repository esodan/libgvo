/*
 * GVo Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2018 <esodan@gmail.com>
 *
 * GVo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GVls is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Tests.UnitTest {
    static int main (string[] args)
    {
        GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
        Test.init (ref args);
        Test.add_func ("/gvo/container-named/default",
        ()=>{
            var n = new GVo.ContainerNamed ();
            var v = n.to_variant ();
            message ("Output:\n%s", v.print (true));

            var o1 = new Tests.PropertyObject ();
            var no1 = new GVo.Named.with_object ("Property1", o1);
            n.add (no1);

            var o2 = new Tests.PropertyObject ();
            o2.my_test = "Other Value 1";
            var no2 = new GVo.Named.with_object ("Property2", o2);
            n.add (no2);

            var v1 = n.to_variant ();
            message ("Output:\n%s", v1.print (true));

            assert (v1.is_container ());
            assert (v1.n_children () == 2);
            assert (v1.lookup_value ("Property1", GLib.VariantType.DICTIONARY) != null);
            assert (v1.lookup_value ("Property2", GLib.VariantType.DICTIONARY) != null);

            var n1 = new GVo.ContainerNamed ();
            n1.parse_variant (v1);
            n1.add_parseable_type (typeof (Tests.PropertyObject));
            var v2 = n1.to_variant ();
            message ("Output:\n%s", v2.print (true));
            assert (v2.is_container ());
            assert (v2.check_format_string ("a{sv}", false));

            var n2 = new GVo.ContainerNamed ();
            n2.add_parseable_type (typeof (Tests.PropertyObject));
            message ("Parsing variant: %s", v1.print (true));
            n2.parse_variant (v1);
            var v3 = n2.to_variant ();
            message ("Output:\n%s", v3.print (true));
            assert (v3.n_children () == 2);
            assert (v3.lookup_value ("Property1", GLib.VariantType.DICTIONARY) != null);
            assert (v3.lookup_value ("Property2", GLib.VariantType.DICTIONARY) != null);

            var os1 = new GVo.String.for_string ("ValString1");
            var ob1 = new GVo.Boolean ();
            ob1.val = true;
            var nos1 = new GVo.Named ();
            nos1.name = "String";
            nos1.object = os1;
            n.add (nos1);
            var nob1 = new GVo.Named ();
            nob1.name = "Boolean";
            nob1.object = ob1;
            n.add (nob1);
            var v4 = n.to_variant ();
            message ("Output:\n%s", v4.print (true));

            var n3 = new GVo.ContainerNamed ();
            n3.add_parseable_type (typeof (Tests.PropertyObject));
            n3.add_parseable_type (typeof (GVo.String));
            n3.add_parseable_type (typeof (GVo.Boolean));
            message ("Parsing variant: %s", v4.print (true));
            n3.parse_variant (v4);
            var v5 = n3.to_variant ();
            message ("Output:\n%s", v5.print (true));
            assert (v5.n_children () == 4);
            assert (v5.lookup_value ("Property1", GLib.VariantType.DICTIONARY) != null);
            assert (v5.lookup_value ("Property2", GLib.VariantType.DICTIONARY) != null);
            assert (v5.lookup_value ("String", GLib.VariantType.STRING) != null);
            assert (v5.lookup_value ("Boolean", GLib.VariantType.BOOLEAN) != null);

            var ob = new Tests.PropertyObjectBool ();
            for (int i = 0; i < 3; i++) {
                var obp = new GVo.Boolean ();
                obp.val = true;
                ob.conditions.add (obp);
            }
            var nobn = new GVo.Named ();
            nobn.name = "List";
            nobn.object = ob;
            n.add (nobn);
            var v6 = n.to_variant ();
            message ("Output:\n%s", v6.print (true));


            var obs = new Tests.PropertyObjectString ();
            for (int i = 0; i < 3; i++) {
                var obsp = new GVo.String.for_string ("str%d".printf (i));
                obs.names.add (obsp);
            }
            var nobsn = new GVo.Named ();
            nobsn.name = "ListNames";
            nobsn.object = obs;
            var n4 = new GVo.ContainerNamed ();
            n4.add (nos1);
            n4.add (nob1);
            n4.add (nobn);
            n4.add (nobsn);
            var v7 = n4.to_variant ();
            message ("Output:\n%s", v7.print (true));

            var n5 = new GVo.ContainerNamed ();
            n5.add_parseable_type (typeof (GVo.String));
            n5.add_parseable_type (typeof (GVo.Boolean));
            n5.add_parseable_type (typeof (Tests.PropertyObjectBool));
            n5.add_parseable_type (typeof (Tests.PropertyObjectString));
            message ("Parsing variant: %s", v7.print (true));
            n5.parse_variant (v7);
            var v8 = n5.to_variant ();
            message ("Output:\n%s", v8.print (true));
            assert (v8.n_children () == 4);
            assert (v8.lookup_value ("ListNames", GLib.VariantType.DICTIONARY) != null);
            assert (v8.lookup_value ("String", GLib.VariantType.STRING) != null);
            assert (v8.lookup_value ("Boolean", GLib.VariantType.BOOLEAN) != null);
            assert (v8.lookup_value ("List", GLib.VariantType.DICTIONARY) != null);
        });

        return Test.run ();
    }
}
