/*
 * GVo Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2018 <esodan@gmail.com>
 *
 * GVo is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GVls is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Tests.UnitTest {
    static int main (string[] args)
    {
        GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
        Test.init (ref args);
        Test.add_func ("/gvo/container-list/default",
        ()=>{
            var n = new GVo.ContainerList ();
            var v = n.to_variant ();
            message ("Output:\n%s", v.print (true));

            var os1 = new GVo.String.for_string ("ValString1");
            var ob1 = new GVo.Boolean ();
            ob1.val = true;
            n.add (os1);
            n.add (ob1);


            var ob = new Tests.PropertyObjectBool ();
            for (int i = 0; i < 3; i++) {
                var obp = new GVo.Boolean ();
                obp.val = true;
                ob.conditions.add (obp);
            }
            n.add (ob);

            var obs = new Tests.PropertyObjectString ();
            for (int i = 0; i < 3; i++) {
                var obsp = new GVo.String.for_string ("str%d".printf (i));
                obs.names.add (obsp);
            }
            n.add (obs);

            var v1 = n.to_variant ();
            message ("Output:\n%s", v1.print (true));

            var n1 = new GVo.ContainerList ();
            n1.add_parseable_type (typeof (GVo.String));
            n1.add_parseable_type (typeof (GVo.Boolean));
            n1.add_parseable_type (typeof (Tests.PropertyObjectBool));
            n1.add_parseable_type (typeof (Tests.PropertyObjectString));
            message ("Parsing variant: %s", v1.print (true));
            n1.parse_variant (v1);
            var v2 = n1.to_variant ();
            message ("Output:\n%s", v2.print (true));
            assert (v2.n_children () == 4);
        });

        return Test.run ();
    }
}
