![logo](GVls_Logo.png "GVls Logo")
# Introduction
Library `libgvo` add serialization to/from Variant/Json for GObject classes.
You don't need to walk across the Variant/Json tree to access properties you
are interested to update your object, `libgvo` works for you.

There is a set of classes and an interface called `GVo.Object` helping
classes to serialize itself to/from `GLib.Variant`, using
class's properties' nickname as the name used when serialized
to JSON files. Vala syntax makes really easy to define
which properties you want to serialize.

# Documentation

You can browse [documentation](https://esodan.pages.gitlab.gnome.org/libgvo) 
generated from latest sources.

# Installation

## Dependencies 

### GObject serialization to JSON

`libgvo` requires:

* meson
* ninja
* glib-2.0
* gio-2.0
* gee-0.8

## Compile
For standard compilation and installation using `/usr` prefix, use:

```
meson _build --prefix=/usr
cd _build
ninja
ninja install
```

If you plan to disable documentation generation use `-Ddocs=false` as
an option to `meson` command.

To disable GObject Introspection bindings use `-Dintrospection=false`

